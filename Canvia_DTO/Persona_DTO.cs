﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canvia_DTO
{
    public class Persona_DTO
    {
        public int intPersonaId { get; set; }
        public string strDni { get; set; }
        public string strNombre { get; set; }
        public int intEdad { get; set; }
        //public bool bitEstado { get; set; }
    }
}
