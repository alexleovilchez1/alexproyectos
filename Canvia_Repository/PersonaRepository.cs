﻿using Canvia_DTO;
using Canvia_Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Canvia_Repository
{
    public class PersonaRepository : IPersonaServices
    {
        private readonly IOptions<ConnectionRepository> cnx;
        public PersonaRepository(IOptions<ConnectionRepository> conexion)
        {
            cnx = conexion;
        }

        public bool Actualizar(int intPersonaId,Persona_DTO BE)
        {
            bool result = false;
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_actualizar", cn))
                {
                    cn.Open();
                    try
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@intPersonaId", SqlDbType.Int).Value = intPersonaId;
                        cmd.Parameters.Add("@strDni", SqlDbType.VarChar, 50).Value = BE.strDni;
                        cmd.Parameters.Add("@strNombre", SqlDbType.VarChar, 50).Value = BE.strNombre;
                        cmd.Parameters.Add("@intEdad", SqlDbType.Int).Value = BE.intEdad;
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return result;
        }

        public bool AnulacionLogica(int intPersonaId)
        {
            bool result = false;
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_anulacionLogica", cn))
                {
                    cn.Open();
                    try
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@intPersonaId", SqlDbType.Int).Value = intPersonaId;
                       
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return result;
        }

        public bool Eliminar(int intPersonaId)
        {
            bool result = false;
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_eliminar", cn))
                {
                    cn.Open();
                    try
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@intPersonaId", SqlDbType.Int).Value = intPersonaId;
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return result;
        }

        public bool Insertar(Persona_DTO BE)
        {
            bool result = false;
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_insert", cn))
                {
                    cn.Open();
                    try
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@strDni", SqlDbType.VarChar, 50).Value = BE.strDni;
                        cmd.Parameters.Add("@strNombre", SqlDbType.VarChar, 50).Value = BE.strNombre;
                        cmd.Parameters.Add("@intEdad", SqlDbType.Int).Value = BE.intEdad;
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return result;
        }

        public List<Persona_DTO> Listar()
        {
            List<Persona_DTO> lista = new List<Persona_DTO>();
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_listar", cn))
                {
                    try
                    {
                        cn.Open();
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                
                                lista.Add(new Persona_DTO()
                                {
                                    intPersonaId = Convert.ToInt32(dr["intPersonaId"].ToString()),
                                    strDni = dr["strDni"].ToString(),
                                    strNombre = dr["strNombre"].ToString(),
                                    intEdad = Convert.ToInt32(dr["intEdad"].ToString()),

                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }
                }
            }
            return lista; throw new NotImplementedException();
        }

        public List<Persona_DTO> ListarById(int intPersonaId)
        {
            List<Persona_DTO> lista = new List<Persona_DTO>();
            using (SqlConnection cn = new SqlConnection(cnx.Value.conexionLocal))
            {
                using (SqlCommand cmd = new SqlCommand("usp_persona_ListarById", cn))
                {
                    try
                    {
                        cn.Open();
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@intPersonaId", SqlDbType.Int).Value = intPersonaId;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lista.Add(new Persona_DTO()
                                {
                                    intPersonaId = Convert.ToInt32(dr["intPersonaId"].ToString()),

                                    strDni = dr["strDni"].ToString(),
                                    strNombre = dr["strNombre"].ToString(),
                                    intEdad = Convert.ToInt32(dr["intEdad"].ToString()),
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                    }
                }
            }
            return lista; throw new NotImplementedException();
        }
    }
}
