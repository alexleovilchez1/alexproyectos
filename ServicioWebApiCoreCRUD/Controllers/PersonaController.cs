﻿using Canvia_DTO;
using Canvia_Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioWebApiCoreCRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        private readonly IPersonaServices _IPersonaServices;
        public PersonaController(IPersonaServices IPersonaServices)
        {
            this._IPersonaServices = IPersonaServices;
        }
        [HttpPost]
        [Route("InsertarPersona")]
        public IActionResult Insertar([FromBody] Persona_DTO BE)
        {
            object result;
            try
            {
                result = new { code = 101, message = "hubo un problema para registrar" };
                var res = _IPersonaServices.Insertar(BE);
                if (res != null)
                {
                    result = new { code = 200, message = "Se registro correctamente", result = true, obj = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("ListarTodo")]
        public IActionResult ListarTodo()
        {
            object result;
            try
            {
                result = new { code = 101, message = "Success, id not found" };

                var res = _IPersonaServices.Listar();
                if (res != null)
                {
                    result = new { code = 200, message = "Success", result = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("ListarById")]
        public IActionResult ListarById(int intPersonaId)
        {
            object result;
            try
            {
                result = new { code = 101, message = "Success, id not found" };
                Persona_DTO BE = new Persona_DTO();
                var res = _IPersonaServices.ListarById(intPersonaId);
                if (res != null)
                {
                    result = new { code = 200, message = "Success", result = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }

        [HttpDelete]
        [Route("Elimminar")]
        public IActionResult Eliminar(int intPersonaId)
        {
            object result;
            try
            {
                result = new { code = 101, message = "Success, id not found" };
                Persona_DTO BE = new Persona_DTO();
                var res = _IPersonaServices.Eliminar(intPersonaId);
                if (res)
                {
                    result = new { code = 200, message = "Success", result = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }

        [HttpPost]
        [Route("AnulacionLogica")]
        public IActionResult AnulacionLogica(int intPersonaId)
        {
            object result;
            try
            {
                result = new { code = 101, message = "hubo un problema para anular" };
                Persona_DTO BE = new Persona_DTO();
                var res = _IPersonaServices.AnulacionLogica(intPersonaId);
                if (res != null)
                {
                    result = new { code = 200, message = "Se Anulo correctamente", result = true, obj = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }

        [HttpPut]
        [Route("Actualizar")]
        public IActionResult Actualizar(int intPersonaId, Persona_DTO BE)
        {

            object result;
            try
            {
                result = new { code = 101, message = "hubo un problema para Actualizar" };
                var res = _IPersonaServices.Actualizar(intPersonaId,BE);
                if (res != null)
                {
                    result = new { code = 200, message = "Se Actualizo correctamente", result = true, obj = res };
                }
            }
            catch (Exception ex)
            {
                result = new { code = -100, message = ex.Message };
            }

            return Ok(result);
        }
    }
}
