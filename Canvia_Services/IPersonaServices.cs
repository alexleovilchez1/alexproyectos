﻿using Canvia_DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Canvia_Services
{
    public interface IPersonaServices
    {

        bool Insertar(Persona_DTO BE);
        bool Eliminar(int intPersonaId);
        bool AnulacionLogica(int intPersonaId);
        bool Actualizar(int intPersonaId,Persona_DTO BE);
        List<Persona_DTO> Listar();
        List<Persona_DTO> ListarById(int intPersonaId);


    }
}
